package net.allegea.crypto;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ RSACipherTests.class, RSAKeyPairTests.class })
public class AllTests {

}
